/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author 66955
 */
public abstract class Animal {

    private String name;
    private int numberOfLegs;

    public Animal(String name, int numberOfLegs) {
        this.name = name;
        this.numberOfLegs = numberOfLegs;
    }

    public String getName() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public int getnumberOfLegs() {
        return numberOfLegs;
    }

    public void setnumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }
    
     @Override
    public String toString() {
        return "Animal{" + "name = " + name +"NumberOfLegs = "+numberOfLegs +"}";
    }
    
    public abstract void eat();
    public abstract void speak();
    public abstract void sleep();
}
