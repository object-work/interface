/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author 66955
 */
public class Test {
    public static void main(String[] args){
        Bat bat = new Bat("vam");
        Plane plane = new Plane("Engine I");
        bat.fly();
        plane.fly();
        
        Dog dog = new Dog("dang");
        Car car = new Car("Engine I");
        
        Flyable[] flyables = {bat,plane};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runables = {dog,car};
        for(Runable r : runables){
            if(r instanceof Car){
                Car c = (Car)r;
                c.startEngine();
            }
            r.run();
        }
        
    } 
}
